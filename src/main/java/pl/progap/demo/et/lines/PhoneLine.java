/**
 * 
 */
package pl.progap.demo.et.lines;


/**
 * @author tbelina
 *
 */
public class PhoneLine extends InputLine {
	private String mobilePhone;
	private String fixedNumber;
	
	@Override
	public InputLine createFromStream(String[] aLine){
		mobilePhone = aLine[1];
		fixedNumber = aLine[2];
		
		return this;
	}

	public PhoneLine(LineType aType) {
		super(aType);
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public String getFixedNumber() {
		return fixedNumber;
	}
	
}

