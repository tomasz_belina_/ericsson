/**
 * 
 */
package pl.progap.demo.et.lines;


/**
 * @author tbelina
 *
 */
public class FamilyLine extends InputLine {
	private String name;
	private String birthYear;
	
	@Override
	public InputLine createFromStream(String[] aLine){
		name = aLine[1];
		birthYear = aLine[2];
		return this;
	}

	public String getName() {
		return name;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public FamilyLine(LineType aType) {
		super(aType);
	}	
}
