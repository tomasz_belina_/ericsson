/**
 * 
 */
package pl.progap.demo.et.lines;

/**
 * @author tbelina
 *
 */
public enum LineType {
	P,
	T,
	A,
	F
}
