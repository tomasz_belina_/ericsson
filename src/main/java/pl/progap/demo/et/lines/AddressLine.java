/**
 * 
 */
package pl.progap.demo.et.lines;


/**
 * @author tbelina
 *
 */
public class AddressLine extends InputLine {
	private String city;
	private String street;
	private String number;
	
	@Override
	public InputLine createFromStream(String[] aLine){
		street = aLine[1];
		city = aLine[2];
		if(aLine.length == 4){
			number = aLine[3];
		}
		return this;
	}

	public AddressLine(LineType aType) {
		super(aType);
	}

	public String getCity() {
		return city;
	}

	public String getStreet() {
		return street;
	}

	public String getNumber() {
		return number;
	}
	
}
