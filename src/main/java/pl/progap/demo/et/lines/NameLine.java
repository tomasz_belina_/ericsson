package pl.progap.demo.et.lines;


/**
 * @author tbelina
 *
 */
public class NameLine extends InputLine {
	private String firstName;
	private String lastName;
	
	@Override
	public InputLine createFromStream(String[] aLine){
		firstName = aLine[1];
		lastName = aLine[2];
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public NameLine(LineType aType) {
		super(aType);
	}
	
}
