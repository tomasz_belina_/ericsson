/**
 * 
 */
package pl.progap.demo.et.lines;

import java.io.IOException;

import pl.progap.demo.et.InputFileFormatException;

/**
 * @author tbelina
 *
 */
public abstract class InputLine {
	private LineType type;

	public InputLine(LineType aType) {
		super();
		type = aType;
	}

	
	public LineType getType() {
		return type;
	}

	/**
	 * Method should fill dao fields from tokenizer
	 * @param aReader
	 * @return
	 * @throws IOException
	 */
	public abstract InputLine createFromStream(String[] aLine);
	
	public static InputLine createLine(LineType aLineType, String[] aLine) throws IOException{
		InputLine pLine;
		switch (aLineType) {
		case P:
			pLine = new NameLine(LineType.P);
			break;
		case T:
			pLine = new PhoneLine(LineType.T);
			break;
		case A:
			pLine = new AddressLine(LineType.A);
			break;
		case F:
			pLine = new FamilyLine(LineType.F);
			break;
		default:
			throw new InputFileFormatException("Unsupported line type");
		}
		pLine.createFromStream(aLine);
		return pLine;
	}
}
