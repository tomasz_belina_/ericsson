/**
 * 
 */
package pl.progap.demo.et;

/**
 * @author tbelina
 *
 */
public class InputFileFormatException extends RuntimeException {

	public InputFileFormatException(String aMessage) {
		super(aMessage);
	}

}
