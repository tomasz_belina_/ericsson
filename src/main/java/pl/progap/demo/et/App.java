package pl.progap.demo.et;

import java.io.IOException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.progap.demo.et.lines.AddressLine;
import pl.progap.demo.et.lines.FamilyLine;
import pl.progap.demo.et.lines.InputLine;
import pl.progap.demo.et.lines.NameLine;
import pl.progap.demo.et.lines.PhoneLine;

/**
 * Main class
 *
 */
public class App 
{
	int level;
	private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static void main( String[] args )
    {
    	
        InputParser pParser = new InputParser(App.class.getResourceAsStream("/test.txt"));
        XMLOutputFactory pOutput = XMLOutputFactory.newInstance();
		try {
	        XMLStreamWriter pWriter = pOutput.createXMLStreamWriter(System.out);
	        new App().transform(pParser, pWriter);
		} catch (XMLStreamException e) {
			logger.error("Exception while generating output stream", e);
		} catch (IOException e) {
			logger.error("Exception while reading input stream", e);
		}
		
		System.out.flush();
        
    }
    public void transform(InputParser aInputParser, XMLStreamWriter aWriter) throws XMLStreamException, IOException{
    	aWriter.writeStartDocument();
    	aWriter.writeStartElement("people");
    	level = 0;
    	InputLine  pLine;
    	while( (pLine = aInputParser.nextLine() )!= null){
    		switch(pLine.getType()){
    		case P: 
    			cleanupLevel(aWriter,level);
    			writePerson((NameLine) pLine, aWriter, aInputParser);    
    			level++;
    			break;
    		case F:
    			if(level > 1){
    				cleanupLevel(aWriter,1);
    			}
    			writeFamily((FamilyLine)pLine, aWriter);
    			level++;
    			break;
    		case T:
    			writePhone((PhoneLine)pLine, aWriter);
    			break;
    		case A:
    			writeAddress((AddressLine)pLine, aWriter);
    			break;
    			
    		}
    	}
    	cleanupLevel(aWriter, level);
    	aWriter.writeEndElement();
    	aWriter.flush();
    }
    
    private void cleanupLevel(XMLStreamWriter aWriter, int aSteps) throws XMLStreamException {
    	for(int i=0; i < aSteps; i++){
    		aWriter.writeEndElement();
    		level--;
    	}
    }
	private void writeAddress(AddressLine aLine, XMLStreamWriter aWriter) throws XMLStreamException {
    	aWriter.writeStartElement("address");
    	writeElement("street", aLine.getStreet(), aWriter);
       	writeElement("city", aLine.getStreet(), aWriter);
       	writeElement("postalCode", aLine.getNumber(), aWriter);
       	aWriter.writeEndElement();

    }
	private void writePhone(PhoneLine aLine, XMLStreamWriter aWriter) throws XMLStreamException {
	  	aWriter.writeStartElement("phone");
    	writeElement("mobile", aLine.getMobilePhone(), aWriter);
       	writeElement("fixed", aLine.getFixedNumber(), aWriter);
       	aWriter.writeEndElement();		
	}
	private void writeFamily(FamilyLine aLine, XMLStreamWriter aWriter) throws XMLStreamException {
	  	aWriter.writeStartElement("family");
    	writeElement("born", aLine.getBirthYear(), aWriter);
       	writeElement("name", aLine.getName(), aWriter);
	}
	private void writePerson(NameLine aLine, XMLStreamWriter aWriter, InputParser aParser) throws XMLStreamException, IOException{
		aWriter.writeStartElement("person");
		writeElement("firstname", aLine.getFirstName(), aWriter);
		writeElement("lastname", aLine.getLastName(), aWriter);
    }
    private void writeElement(String aElementName, String aElementValue, XMLStreamWriter aWriter) throws XMLStreamException{
		aWriter.writeStartElement(aElementName);
		aWriter.writeCharacters(aElementValue);
		aWriter.writeEndElement();
    }
}
