/**
 * 
 */
package pl.progap.demo.et;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import pl.progap.demo.et.lines.InputLine;
import pl.progap.demo.et.lines.LineType;

/**
 * @author tbelina
 *
 */
public class InputParser {
	private BufferedReader reader;
	private static final String TOKEN_SEPARATOR = "\\|";
	private InputLine currentLine;
	public InputParser(InputStream aInputStream){
		reader = new BufferedReader(new InputStreamReader(aInputStream));
	}
	
	/**
	 * Method reads next line from input stream. If stream is empty returns null.
	 * @return
	 */
	public InputLine nextLine() throws IOException{
		String pLine = reader.readLine();
		if(pLine == null){
			currentLine = null;
			return null;
		}
		String[] pSpliedLine = pLine.split(TOKEN_SEPARATOR);
		LineType pLineType = LineType.valueOf(pSpliedLine[0]); //TODO:Handle unknow line types
		currentLine = InputLine.createLine(pLineType, pSpliedLine);
		return currentLine;
	}
}
